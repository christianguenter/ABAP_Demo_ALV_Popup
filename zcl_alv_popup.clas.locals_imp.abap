*"* use this source file for the definition and implementation of
*"* local helper classes, interface definitions and type
*"* declarations


CLASS lcl_null_callback DEFINITION FINAL.

  PUBLIC SECTION.
    INTERFACES: zif_alv_popup_callback.

ENDCLASS.

CLASS lcl_null_callback IMPLEMENTATION.

  METHOD zif_alv_popup_callback~before_display.

  ENDMETHOD.

  METHOD zif_alv_popup_callback~added_function_click.

  ENDMETHOD.

ENDCLASS.
