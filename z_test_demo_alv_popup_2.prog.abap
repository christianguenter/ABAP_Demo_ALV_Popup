*&---------------------------------------------------------------------*
*& Report z_test_demo_alv_popup_2
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT z_test_demo_alv_popup_2.

CLASS controller DEFINITION CREATE PUBLIC.

  PUBLIC SECTION.
    INTERFACES:
      zif_alv_popup_callback.

    METHODS:
      run.

  PRIVATE SECTION.
    METHODS:
      on_confirmed FOR EVENT confirmed OF zcl_alv_popup
        IMPORTING et_data sender.

ENDCLASS.

CLASS controller IMPLEMENTATION.

  METHOD run.

    DATA(popup) = NEW zcl_alv_popup( io_callback = me
                                     i_headline  = |This is a test popup| ).

    SELECT FROM t000
           FIELDS *
           INTO TABLE @DATA(t000_tab).

    SET HANDLER on_confirmed FOR popup.

    popup->popup_to_select( it_data = t000_tab ).

    CALL SCREEN 0100.

  ENDMETHOD.

  METHOD on_confirmed.

    FIELD-SYMBOLS: <table> TYPE STANDARD TABLE.

    ASSIGN et_data TO <table>.
    ASSERT sy-subrc = 0.

    LOOP AT <table> ASSIGNING FIELD-SYMBOL(<data>).

    ENDLOOP.

    cl_demo_output=>display( et_data ).

    cl_gui_cfw=>set_new_ok_code( |Dummy| ).

  ENDMETHOD.

  METHOD zif_alv_popup_callback~added_function_click.

    MESSAGE |{ i_fcode }| TYPE 'I'.

  ENDMETHOD.

  METHOD zif_alv_popup_callback~before_display.

    io_alv->get_functions( )->add_function( name     = |TEST|
                                            icon     = |{ icon_test }|
                                            text     = |Test|
                                            tooltip  = |Test|
                                            position = if_salv_c_alignment=>right ).

  ENDMETHOD.

ENDCLASS.

START-OF-SELECTION.
  NEW controller( )->run( ).

*&---------------------------------------------------------------------*
*& Module PBO OUTPUT
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
MODULE pbo OUTPUT.
ENDMODULE.
*&---------------------------------------------------------------------*
*&      Module  PAI  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE pai INPUT.
  SET SCREEN 0.
ENDMODULE.
