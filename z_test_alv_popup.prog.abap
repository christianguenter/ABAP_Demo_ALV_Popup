*&---------------------------------------------------------------------*
*& Report z_test_alv_popup
*&---------------------------------------------------------------------*
*&
*&---------------------------------------------------------------------*
REPORT z_test_alv_popup.

CLASS controller DEFINITION CREATE PUBLIC.

  PUBLIC SECTION.
    INTERFACES: zif_alv_popup_callback.
    METHODS: run.

  PRIVATE SECTION.
    METHODS:
      on_confirmed FOR EVENT confirmed OF zcl_alv_popup
        IMPORTING
            et_data.

ENDCLASS.

CLASS controller IMPLEMENTATION.

  METHOD run.

*    SELECT FROM t100
*           FIELDS *
*           INTO TABLE @DATA(lt_t100)
*           UP TO 100 ROWS.

    SELECT FROM t000
           FIELDS *
           INTO TABLE @DATA(lt_t000).

    DATA(popup) = NEW zcl_alv_popup( io_callback = me
                                     i_headline  = 'The following Objects have been modified locally. Select the Objects which should be overwritten.'  ).

    SET HANDLER on_confirmed FOR popup.

    popup->popup_to_select( it_data = lt_t000 ).

    WRITE: 'Test'.

  ENDMETHOD.

  METHOD on_confirmed.

    cl_demo_output=>display( et_data ).

  ENDMETHOD.

  METHOD zif_alv_popup_callback~before_display.

    io_alv->get_functions( )->add_function( name     = |Test|
                                            text     = |Test|
                                            tooltip  = |Test|
                                            icon     = |{ icon_test }|
                                            position = if_salv_c_alignment=>right ).

  ENDMETHOD.

  METHOD zif_alv_popup_callback~added_function_click.

    MESSAGE |{ i_fcode }| TYPE 'I'.

  ENDMETHOD.

ENDCLASS.

START-OF-SELECTION.
  NEW controller( )->run( ).
