INTERFACE zif_alv_popup_callback
  PUBLIC .
  METHODS:
    before_display
      IMPORTING
        io_alv       TYPE REF TO cl_salv_table
        io_dialogbox TYPE REF TO cl_gui_dialogbox_container
      RAISING
        cx_salv_error,

    added_function_click
      IMPORTING
        it_data TYPE STANDARD TABLE
        i_fcode TYPE sy-ucomm.

ENDINTERFACE.
