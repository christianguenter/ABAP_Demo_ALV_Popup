CLASS zcl_alv_popup DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    METHODS:
      constructor
        IMPORTING
          io_callback TYPE REF TO zif_alv_popup_callback OPTIONAL
          i_headline  TYPE csequence OPTIONAL,

      popup_to_select
        IMPORTING
          it_data TYPE STANDARD TABLE.

    EVENTS:
      confirmed
        EXPORTING
          VALUE(et_data) TYPE any.

  PRIVATE SECTION.
    TYPES:
      ty_data_table TYPE STANDARD TABLE OF char100 WITH DEFAULT KEY.

    CONSTANTS: BEGIN OF co_fcode,
                 select_all   TYPE string VALUE 'ALL',
                 deselect_all TYPE string VALUE 'NONE',
                 ok           TYPE string VALUE 'GOON',
                 cancel       TYPE string VALUE 'CANC',
               END OF co_fcode,
               co_fieldname_selected TYPE lvc_fname VALUE `SELECTED`.

    DATA: mo_dialog_box          TYPE REF TO cl_gui_dialogbox_container,
          mo_alv                 TYPE REF TO cl_salv_table,
          mr_table               TYPE REF TO data,
          mo_table_descr_et_data TYPE REF TO cl_abap_tabledescr,
          mo_splitter            TYPE REF TO cl_gui_splitter_container,
          mo_callback            TYPE REF TO zif_alv_popup_callback,
          m_headline             TYPE string.

    METHODS:
      _on_close FOR EVENT close OF cl_gui_dialogbox_container
        IMPORTING
            sender,

      _on_select_list_function_click FOR EVENT added_function OF cl_salv_events_table
        IMPORTING
            e_salv_function
            sender,

      _on_select_list_link_click FOR EVENT link_click OF cl_salv_events_table
        IMPORTING column row,

      _create_new_table
        IMPORTING
          it_data TYPE STANDARD TABLE,

      _map_int_to_ext_data
        EXPORTING
          et_data TYPE STANDARD TABLE,

      _create_dialogbox,

      _create_splitter,

      _create_headline,

      _create_alv,

      _select_all,

      _deselect_all,

      _set_all_selections_to
        IMPORTING
          i_new_selection_flag TYPE abap_bool,

      _dispatch_select_all,

      _dispatch_deselect_all,

      _dispach_confirm,

      _dispatch_cancel,

      _dispach_others
        IMPORTING
          i_fcode TYPE syst_ucomm.

ENDCLASS.



CLASS zcl_alv_popup IMPLEMENTATION.


  METHOD constructor.

    IF io_callback IS BOUND.

      mo_callback = io_callback.

    ELSE.

      mo_callback = NEW lcl_null_callback( ).

    ENDIF.

    m_headline = i_headline.

  ENDMETHOD.

  METHOD popup_to_select.

    _create_new_table( it_data ).
    _create_dialogbox( ).
    _create_splitter( ).
    _create_headline( ).
    _create_alv( ).

  ENDMETHOD.

  METHOD _create_new_table.

    DATA: lr_struct TYPE REF TO data.
    FIELD-SYMBOLS: <table> TYPE STANDARD TABLE.

    mo_table_descr_et_data = CAST cl_abap_tabledescr( cl_abap_tabledescr=>describe_by_data( it_data ) ).

    DATA(components) = CAST cl_abap_structdescr( mo_table_descr_et_data->get_table_line_type( ) )->get_components( ).

    INSERT INITIAL LINE INTO components ASSIGNING FIELD-SYMBOL(<component>) INDEX 1.
    ASSERT sy-subrc = 0.

    <component>-name = co_fieldname_selected.
    <component>-type ?= cl_abap_datadescr=>describe_by_name( 'FLAG' ).

    DATA(struct_descr) = cl_abap_structdescr=>create( p_components = components ).
    mo_table_descr_et_data = cl_abap_tabledescr=>create( p_line_type = struct_descr ).

    CREATE DATA mr_table TYPE HANDLE mo_table_descr_et_data.
    ASSIGN mr_table->* TO <table>.
    ASSERT sy-subrc = 0.

    CREATE DATA lr_struct TYPE HANDLE struct_descr.
    ASSIGN lr_struct->* TO FIELD-SYMBOL(<struct>).
    ASSERT sy-subrc = 0.

    LOOP AT it_data ASSIGNING FIELD-SYMBOL(<data>).

      CLEAR: <struct>.
      MOVE-CORRESPONDING <data> TO <struct>.
      INSERT <struct> INTO TABLE <table>.

    ENDLOOP.

  ENDMETHOD.

  METHOD _create_dialogbox.

    mo_dialog_box = NEW cl_gui_dialogbox_container( no_autodef_progid_dynnr = abap_true
                                                    caption                 = |{ m_headline }|
                                                    style = cl_gui_control=>ws_maximizebox
                                                    top                     = 10
                                                    left                    = 10
                                                    width                   = 1280
                                                    height                  = 480 ).

    SET HANDLER _on_close FOR mo_dialog_box.

  ENDMETHOD.

  METHOD _create_splitter.

    mo_splitter = NEW cl_gui_splitter_container( parent                  = mo_dialog_box
                                                 rows                    = 2
                                                 columns                 = 1
                                                 no_autodef_progid_dynnr = abap_true ).

    mo_splitter->set_row_sash(
      EXPORTING
        id                = 1
        type              = cl_gui_splitter_container=>type_movable
        value             = cl_gui_splitter_container=>false
      EXCEPTIONS
        cntl_error        = 1
        cntl_system_error = 2
        OTHERS            = 3 ).

    mo_splitter->set_row_height(
      EXPORTING
        id                = 1
        height            = 5
      EXCEPTIONS
        cntl_error        = 1
        cntl_system_error = 2
        OTHERS            = 3 ).

  ENDMETHOD.

  METHOD _create_headline.

    DATA: data_table   TYPE ty_data_table,
          assigned_url TYPE char100.

    DATA(top_container) = mo_splitter->get_container( row    = 1
                                                      column = 1 ).

    DATA(html_control) = NEW cl_gui_html_viewer( parent = top_container ).

    data_table = VALUE #( ( '<html>' )
                          ( '<body>')
                          ( '<h4 style="font-family:verdana, sans-serif;">' )
                          ( |{ m_headline }| )
                          ( '</h4>')
                          ( '</body>')
                          ( '</html>' ) ).

    html_control->load_data(
      IMPORTING
        assigned_url = assigned_url
      CHANGING
        data_table   = data_table
      EXCEPTIONS
        OTHERS       = 5 ).

    html_control->show_url(
      EXPORTING
        url    = assigned_url
      EXCEPTIONS
        OTHERS = 5 ).

  ENDMETHOD.

  METHOD _create_alv.

    DATA: column TYPE REF TO cl_salv_column_table.

    FIELD-SYMBOLS: <table> TYPE STANDARD TABLE.

    ASSIGN mr_table->* TO <table>.

    DATA(bottom_container) = mo_splitter->get_container( row    = 2
                                                         column = 1 ).

    TRY.
        cl_salv_table=>factory(
          EXPORTING
            r_container    = bottom_container
          IMPORTING
            r_salv_table   = mo_alv
          CHANGING
            t_table        = <table> ).

        DATA(functions) = mo_alv->get_functions( ).

        functions->add_function( name     = |{ co_fcode-select_all }|
                                 tooltip  = |Select all|
                                 icon     = |{ icon_select_all }|
                                 position = if_salv_c_alignment=>left ).

        functions->add_function( name     = |{ co_fcode-deselect_all }|
                                 tooltip  = |Deselect all|
                                 icon     = |{ icon_deselect_all }|
                                 position = if_salv_c_alignment=>left ).

        functions->add_function( name     = |{ co_fcode-ok }|
                                 tooltip  = |Ok|
                                 icon     = |{ icon_okay }|
                                 position = if_salv_c_alignment=>left ).

        functions->add_function( name     = |{ co_fcode-cancel }|
                                 tooltip  = |Cancel|
                                 icon     = |{ icon_cancel }|
                                 position = if_salv_c_alignment=>left ).

        DATA(events) = mo_alv->get_event( ).

        SET HANDLER _on_select_list_link_click FOR events.
        SET HANDLER _on_select_list_function_click FOR events.

        column ?= mo_alv->get_columns( )->get_column( co_fieldname_selected ).
        column->set_cell_type( if_salv_c_cell_type=>checkbox_hotspot ).
        column->set_short_text( |Mark| ).
        column->set_medium_text( |Mark| ).
        column->set_long_text( |Mark| ).
        column->set_output_length( 4 ).

        mo_callback->before_display( io_alv       = mo_alv
                                     io_dialogbox = mo_dialog_box ).

        mo_alv->display( ).

      CATCH cx_salv_error INTO DATA(error).
        MESSAGE error TYPE 'S' DISPLAY LIKE 'E'.
    ENDTRY.

  ENDMETHOD.


  METHOD _on_close.

    mo_dialog_box->set_visible(
      EXPORTING
        visible           = abap_false
      EXCEPTIONS
        cntl_error        = 1
        cntl_system_error = 2
        OTHERS            = 3 ).

    IF sender IS BOUND.
      sender->free( ).
    ENDIF.

  ENDMETHOD.


  METHOD _on_select_list_function_click.

    CASE e_salv_function.
      WHEN co_fcode-select_all.

        _dispatch_select_all( ).

      WHEN co_fcode-deselect_all.

        _dispatch_deselect_all( ).

      WHEN co_fcode-ok.

        _dispach_confirm( ).

      WHEN co_fcode-cancel.

        _dispatch_cancel( ).

      WHEN OTHERS.

        _dispach_others( e_salv_function ).

    ENDCASE.

  ENDMETHOD.


  METHOD _on_select_list_link_click.

    FIELD-SYMBOLS: <table> TYPE STANDARD TABLE.
    ASSIGN mr_table->* TO <table>.

    ASSIGN <table>[ row ] TO FIELD-SYMBOL(<line>).
    IF sy-subrc = 0.

      ASSIGN COMPONENT co_fieldname_selected OF STRUCTURE <line>
             TO FIELD-SYMBOL(<selected>).
      ASSERT sy-subrc = 0.

      <selected> = boolc( <selected> = abap_false ).

    ENDIF.

    mo_alv->refresh( ).

  ENDMETHOD.


  METHOD _map_int_to_ext_data.

    DATA: lr_data TYPE REF TO data.
    FIELD-SYMBOLS: <table> TYPE STANDARD TABLE.

    ASSIGN mr_table->* TO <table>.

    CREATE DATA lr_data LIKE LINE OF et_data.
    ASSIGN lr_data->* TO FIELD-SYMBOL(<e_line>).
    ASSERT sy-subrc = 0.

    DATA(condition) = |{ co_fieldname_selected } = ABAP_TRUE|.

    LOOP AT <table> ASSIGNING FIELD-SYMBOL(<line>)
                    WHERE (condition).

      MOVE-CORRESPONDING <line> TO <e_line>.
      INSERT <e_line> INTO TABLE et_data.

    ENDLOOP.

  ENDMETHOD.


  METHOD _select_all.

    _set_all_selections_to( abap_true ).

  ENDMETHOD.


  METHOD _deselect_all.

    _set_all_selections_to( abap_false ).

  ENDMETHOD.


  METHOD _set_all_selections_to.

    FIELD-SYMBOLS: <table> TYPE STANDARD TABLE.
    ASSIGN mr_table->* TO <table>.

    LOOP AT <table> ASSIGNING FIELD-SYMBOL(<line>).

      ASSIGN COMPONENT co_fieldname_selected OF STRUCTURE <line>
             TO FIELD-SYMBOL(<selected>).
      ASSERT sy-subrc = 0.

      <selected> = i_new_selection_flag.

    ENDLOOP.

  ENDMETHOD.


  METHOD _dispatch_select_all.

    _select_all( ).

    mo_alv->refresh( ).

  ENDMETHOD.


  METHOD _dispatch_deselect_all.

    _deselect_all( ).

    mo_alv->refresh( ).

  ENDMETHOD.


  METHOD _dispach_confirm.

    DATA: lr_table TYPE REF TO data.

    mo_alv->close_screen( ).

    mo_dialog_box->set_visible(
      EXPORTING
        visible           = abap_false
      EXCEPTIONS
        cntl_error        = 1
        cntl_system_error = 2
        OTHERS            = 3 ).

    mo_dialog_box->free( ).

    CREATE DATA lr_table TYPE HANDLE mo_table_descr_et_data.
    ASSIGN lr_table->* TO FIELD-SYMBOL(<table2>).

    _map_int_to_ext_data(
      IMPORTING
        et_data = <table2> ).

    RAISE EVENT confirmed
      EXPORTING
        et_data = <table2>.

  ENDMETHOD.


  METHOD _dispatch_cancel.

    mo_alv->close_screen( ).

    mo_dialog_box->set_visible(
      EXPORTING
        visible           = abap_false
      EXCEPTIONS
        cntl_error        = 1
        cntl_system_error = 2
        OTHERS            = 3 ).

    mo_dialog_box->free( ).

  ENDMETHOD.


  METHOD _dispach_others.

    FIELD-SYMBOLS: <table> TYPE STANDARD TABLE.
    ASSIGN mr_table->* TO <table>.

    mo_callback->added_function_click( it_data = <table>
                                       i_fcode = i_fcode ).

  ENDMETHOD.

ENDCLASS.
